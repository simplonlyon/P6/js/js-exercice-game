Js Exercice Game : (index.html / index.js)
Dans un nouveau projet (fait comme il faut avec npm et tout), faire une page avec un carré rouge dedans, et quand on appuie sur flêches du clavier, le carré se déplace
1. Créer un projet gitlab js-exercice-game, cloner ce projet puis faire un npm init -y dedans
2. Créer la structure de fichier qui va bien, donc un dossier src qui contiendra vos sources, avec un index.html dedans, un dossier css avec style.css dedans et un dossier js avec un index.js dedans(modifié)
3. Dans le index.html, faire une div avec une class red-square, faire le css de ce truc pour que ça soit un carré rouge en position absolute avec un top et un left à 0(modifié)
4. Dans le index.js, capturer l'élément carré rouge et le mettre dans une variable
5. Puis rajouter un event listener sur le body au keydown, en mettant l'argument event dans la fonction
6. Faire dans cet event que la propriété left du carré augmente de 1
     a- Récupérer la position actuelle du carré avec les propriétés offsetLeft et offsetTop sur l'élément carré capturé au dessus
    b- Modifier le style.left du carré en utilisant la valeur récupéré juste avant + 1, concaténée de 'px'


V2 de l'exercice : (v2.html / v2.js)
Dans un couple de nouveaux fichier html/js, faire exactement la même chose, mais cette fois ci, en utilisant une classe et des méthodes(modifié)
1. Créer un fichier square.js, dedans faire une classe Square
2. faire que cette classe ait 2 propriété, x et y, initialisées à 0 par défaut
3. rajouter une méthode moveLeft qui augementera la valeur de x de 1
4. Faire méthode draw (ou toHTML) qui créera un élément HTML représentant le carré
a) faire une variable dans laquelle on met un document.createElement de 'div'
b) modifier la propriété id de cette variable ou lui mettre l'id css correspondant au carré (ou alors la classe, mais avec la propriété className)
c) modifier la propriété style.left pour lui mettre comme valeur this.x concaténé de 'px'
d) Faire que la méthode return la variable(modifié)
5. Créer un fichier v2.html et v2.js, dans le html charger le style.css d'avant et les scripts square.js puis v2.js en bas du body. Créer dans le body une balise avec un id "playfield" (c'est là qu'on mettra le carré)(modifié)
6. Dans v2.js, commencer par créer une variable avec une instance de Square à l'intérieur (new Square() )
7. Puis capturer l'élément playfield avec un querySelector pour le mettre dans une variable aussi
8. Refaire un addEventListener sur le body exactement comme avant sur le keydown
9. Dans cet eventListener, appeler la méthode moveLeft() du carré puis faire un console log du carré pour voir si son x augmente bien
10. En dessous de l'appel de moveLeft(), remettre à zéro le contenu de playfield (avec innerHTML = '')
11. En dessous de la remise à zéro, faire sur le playfield un appendChild de la méthode draw() du carré

Exercice optionnel :
Dans la v2 de l'exercice, commencer par faire en sorte que le carré se déplace dans la direction voulue selon
la touche du clavier que l'on appuie. Ensuite, faire en sorte de pouvoir faire "léviter le carré" quand on appuie
sur la touche espace.
1. Dans la classe Square, rajouter 3 méthode : moveLeft() qui fera diminuer la valeur de this.x, moveUp() qui fera diminuer la valeur de this.y, moveDown() qui fera augmenter la valeur de this.y
2. Essayer ces différentes méthodes en rajoutant à la ligne 3 de v2.js un appel à la méthode sur l'instance newSquare suivie d'un console log de cette instance pour voir si les valeurs x et y augmentent et diminuent bien comme il faut selon la méthode appelée.
3. Dans l'event listener du body, à la place de `newSquare.moveRight()`,  récupérer la touche pressée par l'utilisateur.ice en utilisant l'objet `event` et plus précisemment sa propriété `code`. Faire un console log de la touche pressée. Vous pouvez stocker la valeur en question dans une variable si vous le souhaitez.
4. Toujours dans l'event listener, faire un switch ou une suite de if pour faire une action différente selon la touche pressée : si la touche est 'ArrowUp' faire un console log de 'up', si la touche est 'ArrowDown' console log de 'down', et même principe pour 'ArrowLeft' et 'ArrowRight'
5. Une fois que le bon console log s'affiche pour la bonne touche, remplacer chaque console log par un appel à la méthode correspondante (newSquare.moveUp() pour 'ArrowUp' par exemple)
6. Dans le classe Square, rajouter à l'intérieur du constructeur un `this.flying = false;`, cette propriété représentera si le carré est en train de léviter ou pas.
7. Toujours dans la classe Square, modifier la méthode draw en rajoutant une condition qui dira que si this.flying est true, alors on ajoute la classe "flying" à l'élément squareHTML (avec la propriété classList.add())
8. Enfin, terminez la classe Square en ajoutant une méthode fly() qui fera passer la propriété this.flying de true à false ou de false à true selon sa valeur actuelle.
9. Dans le fichier style.css, rajouter une classe .flying avec une propriété box-shadow qui aura pour valeur `6px 5px 15px grey`
10. De retour dans le v2.js, rajouter dans l'event listener du body une nouvelle condition (ou un nouveau case si vous avez utiliser un switch) pour le cas où la touche pressée est égale à 'Space'
11. Dans cette condition, appeler la méthode fly() de l'instance newSquare
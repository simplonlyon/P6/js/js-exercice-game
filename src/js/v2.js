let newSquare = new Square();
let playfield = document.querySelector('#playfield');

document.body.addEventListener('keydown', function (event) {
    newSquare.moveRight(); // incremente la propriété x de newSquare
    console.log(newSquare);
    playfield.innerHTML = ''; // nettoie mon playfield
    playfield.appendChild(newSquare.draw()); // ajoute à playfield le html généré à partir de la méthode draw de mon newSquare
});
let redSquare = document.querySelector('#red-square');
let body = document.querySelector('body');

body.addEventListener('keydown', function(){

  let leftPosition= redSquare.offsetLeft;
  let nextPosition=leftPosition+1;

  redSquare.style.left=`${nextPosition}px`;

});

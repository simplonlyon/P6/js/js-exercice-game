class Square {
  constructor(x = 0,y = 0){
    this.x = x;
    this.y = y;
  }
  moveRight(){
    this.x++;
  }
  draw(){
    let squareHTML = document.createElement('div');
    squareHTML.setAttribute('id','red-square'); //squareHTML.id = 'red-square';

    squareHTML.style.left =`${this.x}px`;
    squareHTML.style.top =`${this.y}px`;

    return squareHTML;
  }
}